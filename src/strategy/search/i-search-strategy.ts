export interface ISearchStrategy<IEntity> {
  filter(entities: IEntity[], query: string): IEntity[];

  isMatching(storedRecipeItem: IEntity, queryInput: string): boolean;
}
