import { Injectable } from "@angular/core";
import { IRecipe } from "../../state/recipe/i-recipe";
import { ISearchStrategy } from "./i-search-strategy";

@Injectable({
  providedIn: "root",
})
export class RecipeSearchStrategy implements ISearchStrategy<IRecipe> {
  filter(entities: IRecipe[], query: string): IRecipe[] {
    return entities.filter(recipe => this.isMatching(recipe, query));
  }

  isMatching(storedRecipeItem: IRecipe, queryInput: string): boolean {
    const query = queryInput.toLowerCase();

    return storedRecipeItem.title.toLowerCase().indexOf(query) > -1;
  }
}
