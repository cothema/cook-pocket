import { EntityState } from "@ngrx/entity";
import { IMenuRecipeState } from "./menu-recipe/i-menu-recipe-state";
import { IRecipe } from "./recipe/i-recipe";
import { IRecipeState } from "./recipe/i-recipe-state";

export enum FeatureKey {
  recipe = "recipe",
  menuRecipe = "menuRecipe",
}

export interface IState extends EntityState<IRecipe> {
  [FeatureKey.recipe]: IRecipeState;
  [FeatureKey.menuRecipe]: IMenuRecipeState;
}
