import { EntityState } from "@ngrx/entity";
import { IRecipe } from "./i-recipe";

export interface IRecipeState extends EntityState<IRecipe> {}
