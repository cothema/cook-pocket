import { Injectable } from "@angular/core";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { RecipeSearchStrategy } from "../../strategy/search/recipe-search-strategy";
import { AbstractCrudService } from "../_shared/abstract-crud.service";
import { FirestoreEntityStatus } from "../_shared/firestore-entity-status";
import { AuthService } from "../auth/auth.service";
import { FeatureKey, IState } from "../i-state";
import { IRecipe } from "./i-recipe";
import { IRecipeState } from "./i-recipe-state";
import { RecipeActions } from "./recipe.actions";
import { RecipeReducer } from "./recipe.reducer";

@Injectable({
  providedIn: "root",
})
export class RecipeService extends AbstractCrudService<IRecipe, IRecipeState> {
  myAuthorRecipes$: Observable<IRecipe[]> = this.store
    .select(rootState => rootState && rootState[FeatureKey.recipe])
    .pipe(
      select((featureState: IRecipeState) => {
        return RecipeReducer.adapter.getSelectors().selectAll(featureState);
      }),
    );

  protected actions = RecipeActions;
  protected reducer = RecipeReducer;
  protected featureKey = FeatureKey.recipe;

  constructor(
    private auth: AuthService,
    searchStrategy: RecipeSearchStrategy,
    store: Store<IState>,
  ) {
    super(store, searchStrategy);
  }

  entityFactory(): IRecipe {
    return {
      author: this.auth.getUserRef(),
      isPublic: false,
      status: FirestoreEntityStatus.ACTIVE,
    };
  }
}
