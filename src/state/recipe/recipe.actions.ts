import { createAction, props } from "@ngrx/store";
import { FeatureKey } from "../i-state";
import { IRecipe } from "./i-recipe";

const featureKey = FeatureKey.recipe;

export class RecipeActions {
  static query = createAction(`[${featureKey}] query`);
  static added = createAction(`[${featureKey}] added`);
  static queryOk = createAction(`[${featureKey}] query ok`);
  static queryErr = createAction(`[${featureKey}] query err`);
  static create = createAction(
    `[${featureKey}] create`,
    props<{ payload: IRecipe }>(),
  );
  static createOk = createAction(`[${featureKey}] create ok`);
  static createErr = createAction(`[${featureKey}] create err`);
  static update = createAction(
    `[${featureKey}] update`,
    props<{ payload: IRecipe }>(),
  );
  static updateOk = createAction(`[${featureKey}] update ok`);
  static updateErr = createAction(`[${featureKey}] update err`);
  static softDelete = createAction(
    `[${featureKey}] soft delete`,
    props<{ payload: { id: string } }>(),
  );
  static softDeleteOk = createAction(`[${featureKey}] soft delete ok`);
  static softDeleteErr = createAction(`[${featureKey}] soft delete err`);
}
