import { createEntityAdapter } from "@ngrx/entity";
import { ActionReducer, createReducer, on } from "@ngrx/store";
import { IState } from "../i-state";
import { IRecipe } from "./i-recipe";
import { IRecipeState } from "./i-recipe-state";
import { RecipeActions } from "./recipe.actions";

export class RecipeReducer {
  public static readonly adapter = createEntityAdapter<IRecipe>();
  public static readonly initialState = RecipeReducer.adapter.getInitialState();
  private static readonly reducer: ActionReducer<IRecipeState> = createReducer(
    RecipeReducer.initialState,
    on(RecipeActions.added, (state: IState, action: any) =>
      RecipeReducer.adapter.addOne(action.payload, state),
    ),
  );

  /**
   * Method returns new state
   *
   * @param state
   * @param action
   */
  static reduce(state = RecipeReducer.initialState, action) {
    return RecipeReducer.reducer(state, action);
  }
}
