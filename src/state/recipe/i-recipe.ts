import { FirestoreEntityStatus } from "../_shared/firestore-entity-status";

export interface IRecipe {
  id?: string;
  title?: string;
  description?: string;
  cookTime?: number; // seconds
  author?: string; // reference to user
  isPublic?: boolean;
  createdAt?: Date;
  deletedAt?: Date;
  lastUpdatedAt?: Date;
  status?: FirestoreEntityStatus;
}
