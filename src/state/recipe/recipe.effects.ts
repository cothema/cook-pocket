import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Actions, Effect } from "@ngrx/effects";
import { Action } from "@ngrx/store";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { Observable } from "rxjs";
import { AbstractCrudEffects } from "../_shared/abstract-crud.effects";
import { IRecipe } from "./i-recipe";
import { IRecipeState } from "./i-recipe-state";
import { RecipeActions } from "./recipe.actions";

@Injectable({
  providedIn: "root",
})
export class RecipeEffects extends AbstractCrudEffects<IRecipe, IRecipeState> {
  protected actions = RecipeActions;
  protected collectionKey = "recipes";

  @Effect()
  query$: Observable<Action> = this.queryPipe();

  @Effect()
  create$: Observable<Action> = this.createPipe();

  @Effect()
  softDelete$: Observable<Action> = this.softDeletePipe();

  @Effect()
  update$: Observable<Action> = this.updatePipe();

  constructor(
    actions$: Actions,
    angularFirestore: AngularFirestore,
    baseAuthService: BaseAuthService,
  ) {
    super(actions$, angularFirestore, baseAuthService);
  }

  protected queryFilter(ref) {
    return ref.where(
      "author",
      "==",
      this.getRef("users/" + this.baseAuthService.user.uid),
    );
  }

  protected mapToFirestore(entityInput: IRecipe): any {
    let entity = super.mapToFirestore(entityInput);

    entity.author = this.getRef(entity.author);

    return entity;
  }

  protected mapFromFirestore(responseData: any): Partial<IRecipe> {
    return {
      title: responseData?.title,
      description: responseData?.description,
      cookTime: responseData?.cookTime,
      author: responseData?.author?.path,
      isPublic: responseData?.isPublic,
      status: responseData?.status,
      deletedAt: responseData?.status,
      lastUpdatedAt: responseData?.lastUpdatedAt,
      createdAt: responseData?.createdAt,
    };
  }
}
