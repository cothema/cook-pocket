import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { User } from "../../model/user";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  constructor(
    public auth: BaseAuthService,
    private angularFirestore: AngularFirestore,
  ) {}

  public getUserRef(): string {
    return this.angularFirestore
      .collection(`users`)
      .doc<User>(this.auth.user.uid).ref.path;
  }
}
