import { AngularFirestore } from "@angular/fire/firestore";
import { Actions, ofType } from "@ngrx/effects";
import { firestore } from "firebase";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { EMPTY, Observable } from "rxjs";
import { catchError, map, mergeMap, switchMap } from "rxjs/operators";
import { FeatureKey } from "../i-state";
import { FirestoreEntityStatus } from "./firestore-entity-status";

export abstract class AbstractCrudEffects<IEntity, IEntityState> {
  protected abstract actions;
  protected abstract collectionKey: string;

  protected constructor(
    protected actions$: Actions,
    protected angularFirestore: AngularFirestore,
    protected baseAuthService: BaseAuthService,
  ) {}

  protected queryFilter(ref) {
    return ref;
  }

  protected queryPipe(): Observable<any> {
    return this.actions$.pipe(
      ofType(this.actions.query),
      switchMap(() => {
        return this.angularFirestore
          .collection<IEntity>(this.collectionKey, ref =>
            this.queryFilter(
              ref.where("status", "in", [
                FirestoreEntityStatus.ACTIVE,
                FirestoreEntityStatus.ARCHIVED,
                FirestoreEntityStatus.DRAFT,
              ]),
            ),
          )
          .stateChanges();
      }),
      mergeMap(actions => actions),
      map(action => {
        return {
          type: `[${FeatureKey.recipe}] ${action.type}`,
          payload: {
            ...this.mapFromFirestore(action.payload.doc.data()),
            id: action.payload.doc.id,
          } as any,
        };
      }),
      catchError(() => EMPTY),
    );
  }

  protected createPipe(): Observable<any> {
    return this.actions$.pipe(
      ofType(this.actions.create),
      mergeMap(async action => {
        try {
          let entity = this.mapToFirestore(action.payload);
          entity.createdAt = new Date();
          await this.angularFirestore
            .collection(this.collectionKey)
            .add(entity);
          return this.actions.createOk();
        } catch (err) {
          console.error(err);
          return this.actions.createErr();
        }
      }),
    );
  }

  protected softDeletePipe(): Observable<any> {
    return this.actions$.pipe(
      ofType(this.actions.softDelete),
      mergeMap(async action => {
        try {
          await this.angularFirestore
            .collection(this.collectionKey)
            .doc(action.payload.id)
            .set(
              { deletedAt: new Date(), status: FirestoreEntityStatus.DELETED },
              { merge: true },
            );
          return this.actions.softDeleteOk();
        } catch (err) {
          console.error(err);
          return this.actions.softDeleteErr();
        }
      }),
    );
  }

  protected updatePipe(): Observable<any> {
    return this.actions$.pipe(
      ofType(this.actions.update),
      mergeMap(async action => {
        try {
          let entity = this.mapToFirestore(action.payload);
          entity.lastUpdatedAt = new Date();
          await this.angularFirestore
            .collection(this.collectionKey)
            .doc(action.payload.id)
            .set(this.mapToFirestore(action.payload), { merge: true });
          return this.actions.updateOk();
        } catch (err) {
          console.error(err);
          return this.actions.updateErr();
        }
      }),
    );
  }

  protected getRef(path: string): firestore.DocumentReference {
    return this.angularFirestore.doc(path).ref;
  }

  protected mapToFirestore(entityInput: IEntity): any {
    let entity: any = Object.assign({}, entityInput);
    entity = JSON.parse(JSON.stringify(entity));
    return entity;
  }

  protected mapFromFirestore(responseData: any): Partial<IEntity> {
    return responseData;
  }
}
