import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ISearchStrategy } from "../../strategy/search/i-search-strategy";
import { IState } from "../i-state";
import { IRecipe } from "../recipe/i-recipe";
import { IRecipeState } from "../recipe/i-recipe-state";

interface IIdentifiedEntity {
  id?: string;
}

export abstract class AbstractCrudService<
  IEntity extends IIdentifiedEntity,
  IEntityState
> {
  protected initialized: boolean = false;
  protected abstract actions;
  protected abstract reducer;
  protected abstract featureKey: string;

  protected constructor(
    protected store: Store<IState>,
    protected searchStrategy?: ISearchStrategy<IEntity>,
  ) {}

  getAllEntities(): Observable<IEntity[] | undefined> {
    return this.store
      .select(rootState => rootState && rootState[this.featureKey])
      .pipe(
        select((featureState: IEntityState) => {
          return this.reducer.adapter.getSelectors().selectAll(featureState);
        }),
      );
  }

  getEntityById(id: string): Observable<IEntity | undefined> {
    return this.getAllEntities().pipe(
      map((entities: IEntity[]) => {
        return entities.find(entity => entity.id === id);
      }),
    );
  }

  getEntityBy(findBy: {}): Observable<IEntity[] | undefined> {
    return this.getAllEntities().pipe(
      map((entities: IEntity[]) => {
        return entities.filter(entity => {
          for (let key in Object.keys(findBy)) {
            if (entity[key] === findBy[key]) {
              return false;
            }
          }
          return true;
        });
      }),
    );
  }

  search(query: string): Observable<IRecipe[] | undefined> {
    if (this.searchStrategy === undefined) {
      console.error("Search not supported!");
    }
    return this.store
      .select(rootState => rootState && rootState[this.featureKey])
      .pipe(
        select((featureState: IRecipeState) => {
          return this.searchStrategy.filter(
            this.reducer.adapter.getSelectors().selectAll(featureState),
            query,
          );
        }),
      );
  }

  entityFactory(): IEntity {
    return {} as IEntity;
  }

  async init(): Promise<void> {
    if (this.initialized) {
      return;
    }
    await this.store.dispatch(this.actions.query());
    this.initialized = true;
  }

  async createEntity(entity: IEntity) {
    await this.store.dispatch(this.actions.create({ payload: entity }));
  }

  async softDeleteEntity(entity: { id: string } | string) {
    const entityId = typeof entity === "object" ? entity.id : entity;
    await this.store.dispatch(
      this.actions.softDelete({ payload: { id: entityId } }),
    );
  }

  async updateEntity(entity: IEntity) {
    await this.store.dispatch(
      this.actions.update({ payload: Object.assign({}, entity) }),
    );
  }
}
