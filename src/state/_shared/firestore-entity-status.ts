export enum FirestoreEntityStatus {
  DELETED = 0,
  ACTIVE = 1,
  ARCHIVED = 2,
  DRAFT = 3,
}
