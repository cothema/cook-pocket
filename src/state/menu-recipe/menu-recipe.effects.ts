import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Actions } from "@ngrx/effects";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { AbstractCrudEffects } from "../_shared/abstract-crud.effects";
import { IMenuRecipe } from "./i-menu-recipe";
import { IMenuRecipeState } from "./i-menu-recipe-state";

@Injectable({
  providedIn: "root",
})
export class RecipeEffects extends AbstractCrudEffects<
  IMenuRecipe,
  IMenuRecipeState
> {
  constructor(
    actions$: Actions,
    angularFirestore: AngularFirestore,
    baseAuthService: BaseAuthService,
  ) {
    super(actions$, angularFirestore, baseAuthService);
  }

  protected mapFromFirestore(responseData: any): Partial<IMenuRecipe> {
    return {
      userRef: responseData?.user?.path,
      createdAt: responseData?.createdAt,
    };
  }
}
