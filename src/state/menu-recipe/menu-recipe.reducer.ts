import { createEntityAdapter } from "@ngrx/entity";
import { ActionReducer, createReducer, on } from "@ngrx/store";
import { IState } from "../i-state";
import { IMenuRecipe } from "./i-menu-recipe";
import { IMenuRecipeState } from "./i-menu-recipe-state";
import { MenuRecipeActions } from "./menu-recipe.actions";

export class MenuRecipeReducer {
  public static readonly adapter = createEntityAdapter<IMenuRecipe>();
  public static readonly initialState = MenuRecipeReducer.adapter.getInitialState();
  private static readonly reducer: ActionReducer<
    IMenuRecipeState
  > = createReducer(
    MenuRecipeReducer.initialState,
    on(MenuRecipeActions.added, (state: IState, action: any) =>
      MenuRecipeReducer.adapter.addOne(action.payload, state),
    ),
  );

  /**
   * Method returns new state
   *
   * @param state
   * @param action
   */
  static reduce(state = MenuRecipeReducer.initialState, action) {
    return MenuRecipeReducer.reducer(state, action);
  }
}
