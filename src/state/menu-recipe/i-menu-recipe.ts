export interface IMenuRecipe {
  id?: string;
  recipeRef?: string;
  userRef?: string;
  scheduledForCookingAt?: Date;
  createdAt?: Date;
  deletedAt?: Date;
  lastUpdatedAt?: Date;
}
