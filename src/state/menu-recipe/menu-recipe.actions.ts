import { createAction, props } from "@ngrx/store";
import { FeatureKey } from "../i-state";
import { IMenuRecipe } from "./i-menu-recipe";

const featureKey = FeatureKey.menuRecipe;

export class MenuRecipeActions {
  static query = createAction(`[${featureKey}] query`);
  static added = createAction(`[${featureKey}] added`);
  static queryOk = createAction(`[${featureKey}] query ok`);
  static queryErr = createAction(`[${featureKey}] query err`);
  static create = createAction(
    `[${featureKey}] create`,
    props<{ payload: IMenuRecipe }>(),
  );
  static createOk = createAction(`[${featureKey}] create ok`);
  static createErr = createAction(`[${featureKey}] create err`);
  static update = createAction(
    `[${featureKey}] update`,
    props<{ payload: IMenuRecipe }>(),
  );
  static updateOk = createAction(`[${featureKey}] update ok`);
  static updateErr = createAction(`[${featureKey}] update err`);
  static delete = createAction(
    `[${featureKey}] delete`,
    props<{ payload: { id: string } }>(),
  );
  static deleteOk = createAction(`[${featureKey}] delete ok`);
  static deleteErr = createAction(`[${featureKey}] delete err`);
}
