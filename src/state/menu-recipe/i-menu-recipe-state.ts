import { EntityState } from "@ngrx/entity";
import { IMenuRecipe } from "./i-menu-recipe";

export interface IMenuRecipeState extends EntityState<IMenuRecipe> {}
