import { Injectable } from "@angular/core";
import { select, Store } from "@ngrx/store";
import { Observable } from "rxjs";
import { AbstractCrudService } from "../_shared/abstract-crud.service";
import { AuthService } from "../auth/auth.service";
import { FeatureKey, IState } from "../i-state";
import { IMenuRecipe } from "./i-menu-recipe";
import { IMenuRecipeState } from "./i-menu-recipe-state";
import { IRecipe } from "./i-recipe";
import { MenuRecipeActions } from "./menu-recipe.actions";
import { MenuRecipeReducer } from "./menu-recipe.reducer";
import { RecipeReducer } from "./recipe.reducer";

@Injectable({
  providedIn: "root",
})
export class MenuRecipeService extends AbstractCrudService<IMenuRecipe, IMenuRecipeState> {
  scheduledRecipes$: Observable<IMenuRecipe[]> = this.store
    .select(rootState => rootState && rootState[FeatureKey.menuRecipe])
    .pipe(
      select((featureState: IMenuRecipeState) => {
        return RecipeReducer.adapter.getSelectors().selectAll(featureState);
      }),
    );

  protected actions = MenuRecipeActions;
  protected reducer = MenuRecipeReducer;

  constructor(
    private auth: AuthService,
    store: Store<IState>
  ) {
    super(store);
  }

  entityFactory(): IRecipe {
    return {
      user: this.auth.getUserRef(),
    };
  }
}
