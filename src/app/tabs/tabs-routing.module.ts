import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs.page";

const routes: Routes = [
  {
    path: "pocket",
    component: TabsPage,
    children: [
      {
        path: "list",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../recipe/recipe-list/recipe-list.module").then(
                m => m.RecipeListPageModule,
              ),
          },
        ],
      },
      {
        path: "menu",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../food-menu/food-menu.module").then(
                m => m.FoodMenuPageModule,
              ),
          },
        ],
      },
      {
        path: "shopping",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../shopping/shopping.module").then(
                m => m.ShoppingPageModule,
              ),
          },
        ],
      },
      {
        path: "recipe",
        children: [
          {
            path: "add",
            loadChildren: () =>
              import("../recipe/recipe-add/recipe-add.module").then(
                m => m.RecipeAddPageModule,
              ),
          },
          {
            path: "detail/:id",
            loadChildren: () =>
              import("../recipe/recipe-detail/recipe-detail.module").then(
                m => m.RecipeDetailPageModule,
              ),
          },
          {
            path: "edit/:id",
            loadChildren: () =>
              import("../recipe/recipe-edit/recipe-edit.module").then(
                m => m.RecipeEditPageModule,
              ),
          },
        ],
      },
      {
        path: "settings",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../settings/settings.module").then(
                m => m.SettingsPageModule,
              ),
          },
        ],
      },
      {
        path: "stats",
        children: [
          {
            path: "",
            loadChildren: () =>
              import("../stats/stats.module").then(
                m => m.StatsPageModule,
              ),
          },
        ],
      },
      {
        path: "",
        redirectTo: "/pocket/list",
        pathMatch: "full",
      },
    ],
  },
  {
    path: "",
    redirectTo: "/pocket/list",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
