import { Component } from "@angular/core";
import { FeatureLevelService } from "../../services/feature-level.service";

@Component({
  selector: "app-tabs",
  templateUrl: "tabs.page.html",
  styleUrls: ["tabs.page.scss"],
})
export class TabsPage {
  constructor(public featureLevel: FeatureLevelService) {}
}
