import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ModalController } from "@ionic/angular";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { User } from "../../../model/user";
import { FirestoreEntityStatus } from "../../../state/_shared/firestore-entity-status";
import { IRecipe } from "../../../state/recipe/i-recipe";
import { RecipeService } from "../../../state/recipe/recipe.service";
import { RecipeDetailPage } from "../recipe-detail/recipe-detail.page";

@Component({
  selector: "app-recipe-list-page",
  templateUrl: "recipe-list.page.html",
  styleUrls: ["recipe-list.page.scss"],
})
export class RecipeListPage implements OnInit {
  searchResult?: IRecipe[];
  searchQuery?: string;

  constructor(
    public auth: BaseAuthService<User>,
    public recipeService: RecipeService,
    public router: Router,
  ) {}

  async ngOnInit() {
    await this.recipeService.init();
  }

  ionViewWillEnter() {
    this.resetSearch();
  }

  onSearchInput($event) {
    if ($event.target.value === "") {
      this.searchResult = undefined;
      return;
    }

    requestAnimationFrame(() => {
      this.recipeService
        .search($event.target.value)
        .subscribe(searchResult => (this.searchResult = searchResult));
    });
  }

  async onOpenDetail(id: string) {
    await this.router.navigate(["pocket/recipe/detail", id]);
  }

  isEntityShowable(entity: IRecipe): boolean {
    return (
      entity &&
      // !entity.deletedAt &&
      [
        FirestoreEntityStatus.DRAFT,
        FirestoreEntityStatus.ARCHIVED,
        FirestoreEntityStatus.ACTIVE,
      ].includes(entity.status)
    );
  }

  resetSearch() {
    this.searchQuery = undefined;
    this.searchResult = undefined;
  }
}
