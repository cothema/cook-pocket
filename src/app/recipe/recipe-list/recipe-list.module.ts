import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "../../shared.module";
import { RecipeListPage } from "./recipe-list.page";

@NgModule({
  imports: [
    SharedModule,
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: "", component: RecipeListPage }]),
  ],
  declarations: [RecipeListPage],
})
export class RecipeListPageModule {}
