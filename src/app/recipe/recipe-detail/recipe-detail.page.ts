import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ImageUploaderService } from "../../../services/image-uploader.service";
import { IRecipe } from "../../../state/recipe/i-recipe";
import { RecipeService } from "../../../state/recipe/recipe.service";

@Component({
  selector: "app-recipe-detail",
  templateUrl: "./recipe-detail.page.html",
  styleUrls: ["./recipe-detail.page.scss"],
})
export class RecipeDetailPage implements OnInit {
  entity: IRecipe;

  constructor(
    private router: Router,
    private recipeService: RecipeService,
    private activatedRoute: ActivatedRoute,
    private imageUploaderService: ImageUploaderService,
  ) {}

  async ngOnInit() {
    await this.recipeService.init();

    this.activatedRoute.params.subscribe(params => {
      this.recipeService.getEntityById(params["id"]).subscribe(entity => {
        this.entity = entity;
      });
    });
  }

  async onScheduleCooking() {
    await this.router.navigate(["pocket/menu"]);
  }

  async onDelete() {
    await this.recipeService.softDeleteEntity(this.entity.id);
    await this.router.navigate(["pocket/list"]);
  }

  async onEdit() {
    await this.router.navigate(["pocket/recipe/edit", this.entity.id]);
  }

  onPrint() {
    window.print();
  }

  onUploadImage() {
    this.imageUploaderService.openImagePicker();
  }
}
