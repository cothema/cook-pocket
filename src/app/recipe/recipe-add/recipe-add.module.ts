import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "../../shared.module";
import { RecipeAddPage } from "./recipe-add.page";

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([{ path: "", component: RecipeAddPage }]),
  ],
  declarations: [RecipeAddPage],
})
export class RecipeAddPageModule {}
