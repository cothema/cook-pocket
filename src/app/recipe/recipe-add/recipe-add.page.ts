import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { User } from "../../../model/user";
import { FeatureLevelService } from "../../../services/feature-level.service";
import { IRecipe } from "../../../state/recipe/i-recipe";
import { RecipeService } from "../../../state/recipe/recipe.service";

@Component({
  selector: "app-recipe-add",
  templateUrl: "./recipe-add.page.html",
  styleUrls: ["./recipe-add.page.scss"],
})
export class RecipeAddPage implements OnInit {
  entity: IRecipe;

  constructor(
    public auth: BaseAuthService<User>,
    public featureLevel: FeatureLevelService,
    public router: Router,
    public recipeService: RecipeService,
  ) {
    this.initBlankEntity();
  }

  async ngOnInit() {}

  private initBlankEntity() {
    this.entity = this.recipeService.entityFactory();
  }

  async onSave() {
    await this.recipeService.createEntity(this.entity);
    await this.router.navigate(["/pocket/list"]);
    this.initBlankEntity();
  }
}
