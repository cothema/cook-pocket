import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { User } from "../../../model/user";
import { FeatureLevelService } from "../../../services/feature-level.service";
import { IRecipe } from "../../../state/recipe/i-recipe";
import { RecipeService } from "../../../state/recipe/recipe.service";

@Component({
  selector: "app-recipe-edit",
  templateUrl: "./recipe-edit.page.html",
  styleUrls: ["./recipe-edit.page.scss"],
})
export class RecipeEditPage implements OnInit {
  entity: IRecipe;

  constructor(
    public auth: BaseAuthService<User>,
    public featureLevel: FeatureLevelService,
    public router: Router,
    public recipeService: RecipeService,
    private activatedRoute: ActivatedRoute,
  ) {
    this.initBlankEntity();
  }

  private initBlankEntity() {
    this.entity = this.recipeService.entityFactory();
  }

  async ngOnInit() {
    await this.recipeService.init();

    this.activatedRoute.params.subscribe(params => {
      this.recipeService.getEntityById(params["id"]).subscribe(entity => {
        this.entity = entity;
      });
    });
  }

  async onSave() {
    await this.recipeService.updateEntity(this.entity);
    await this.router.navigate(["/pocket/recipe/detail/", this.entity.id]);
    this.initBlankEntity();
  }
}
