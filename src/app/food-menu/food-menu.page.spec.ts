import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { IonicModule } from "@ionic/angular";

import { FoodMenuPage } from "./food-menu.page";

describe("FoodMenuPage", () => {
  let component: FoodMenuPage;
  let fixture: ComponentFixture<FoodMenuPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FoodMenuPage],
      imports: [IonicModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(FoodMenuPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
