import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { User } from "../../model/user";
import { IRecipe } from "../../state/recipe/i-recipe";

@Component({
  selector: "app-food-menu-page",
  templateUrl: "food-menu.page.html",
  styleUrls: ["food-menu.page.scss"],
})
export class FoodMenuPage {
  menuRecipes: IRecipe[] = [
    {
      id: "VJS5cU1vyjPEi4qhL9PI",
      title: "Quesadilla",
      cookTime: 5 * 60,
    },
    {
      id: "dkit4EY8dmaVNukCzjHF",
      title: "Salad",
    },
  ];

  constructor(public auth: BaseAuthService<User>, public router: Router) {}

  async onOpenDetail(id: string) {
    await this.router.navigate(["pocket/recipe/detail", id]);
  }
}
