import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "../shared.module";
import { FoodMenuPage } from "./food-menu.page";

@NgModule({
  imports: [
    SharedModule,
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: "", component: FoodMenuPage }]),
  ],
  declarations: [FoodMenuPage],
})
export class FoodMenuPageModule {}
