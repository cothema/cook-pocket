import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { User } from "../../model/user";
import { FeatureLevelService } from "../../services/feature-level.service";

@Component({
  selector: "app-settings",
  templateUrl: "./settings.page.html",
  styleUrls: ["./settings.page.scss"],
})
export class SettingsPage implements OnInit {
  constructor(
    public auth: BaseAuthService<User>,
    public featureLevel: FeatureLevelService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {}

  async onChange(): Promise<void> {
    await this.auth.updateUserData(this.auth.user);
  }

  async onChangeLanguage() {
    await this.onChange();
    this.translate.use(this.auth.user.lang);
  }

  async onChangeFeatureLevel() {
    await this.onChange();
    this.featureLevel.currentLevel = this.auth.user.featureLevel;
  }
}
