import { Component } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Platform } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { User } from "../model/user";
import { FeatureLevelService } from "../services/feature-level.service";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private translate: TranslateService,
    private auth: BaseAuthService<User>,
    private featureLevel: FeatureLevelService,
  ) {
    this.initializeApp();
  }

  async initializeApp() {
    await this.platform.ready();
    this.statusBar.styleDefault();

    this.auth.userInitialized$.subscribe(isInitialized => {
      if (isInitialized) {
        if (this.auth.user !== undefined && this.auth.user) {
          if (this.auth.user.lang) {
            this.translate.use(this.auth.user?.lang);
          } else if (this.translate.getBrowserLang()) {
            this.auth.user.lang = this.translate.getBrowserLang();
            this.auth.updateUserData(this.auth.user);
          }

          if (this.auth.user.featureLevel) {
            this.featureLevel.currentLevel = this.auth.user.featureLevel;
          } else {
            this.auth.user.featureLevel = this.featureLevel.currentLevel;
            this.auth.updateUserData(this.auth.user);
          }
        }
      }
    });

    if (this.translate.getBrowserLang()) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use("en");
    }

    this.splashScreen.hide();
  }
}
