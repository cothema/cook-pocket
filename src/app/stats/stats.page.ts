import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { IStats } from "../../model/i-stats";
import { RecipeService } from "../../state/recipe/recipe.service";

@Component({
  selector: "app-stats-page",
  templateUrl: "stats.page.html",
  styleUrls: ["stats.page.scss"],
})
export class StatsPage implements OnInit {
  stats: IStats = {
    recipesCount: undefined,
  };

  constructor(
    public router: Router,
    public recipeService: RecipeService,
  ) {
  }

  async ngOnInit(): Promise<void> {
    this.recipeService.getAllEntities().subscribe(entities => {
      this.stats.recipesCount = entities.length;
    });
  }
}
