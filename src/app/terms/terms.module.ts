import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "../shared.module";
import { TermsPageRoutingModule } from "./terms-routing.module";
import { TermsPage } from "./terms.page";

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    IonicModule,
    TermsPageRoutingModule,
  ],
  declarations: [TermsPage],
})
export class TermsPageModule {}
