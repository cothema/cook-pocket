import { Component } from "@angular/core";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { BasicCookItem } from "../../model/basic-cook-item";
import { User } from "../../model/user";

@Component({
  selector: "app-shopping-page",
  templateUrl: "shopping.page.html",
  styleUrls: ["shopping.page.scss"],
})
export class ShoppingPage {
  toBuyList: BasicCookItem[] = [
    new BasicCookItem({
      title: "Salad",
    }),
    new BasicCookItem({
      title: "Egg",
    }),
  ];

  constructor(public auth: BaseAuthService<User>) {}
}
