import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { SharedModule } from "../shared.module";
import { SignInPageRoutingModule } from "./sign-in-routing.module";
import { SignInPage } from "./sign-in.page";

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    SignInPageRoutingModule,
  ],
  declarations: [SignInPage],
})
export class SignInPageModule {}
