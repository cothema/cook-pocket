import { Component, OnInit } from "@angular/core";
import { faGoogle } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faGhost } from "@fortawesome/free-solid-svg-icons";
import { BaseAuthService } from "ionic-universal-firebase-login";
import { FeatureLevelService } from "../../services/feature-level.service";

@Component({
  selector: "app-sign-in",
  templateUrl: "./sign-in.page.html",
  styleUrls: ["./sign-in.page.scss"],
})
export class SignInPage implements OnInit {
  faGhost = faGhost;
  faEnvelope = faEnvelope;
  faGoogle = faGoogle;

  constructor(
    public auth: BaseAuthService,
    public featureLevel: FeatureLevelService,
  ) {}

  ngOnInit() {}
}
