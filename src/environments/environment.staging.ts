export const environment = {
  staging: true,
  production: true,
  firebase: {
    apiKey: "AIzaSyA2sm98U9AV9MWtorC246UYJ0PQGsHe6qc",
    authDomain: "cook-pocket.firebaseapp.com",
    databaseURL: "https://cook-pocket.firebaseio.com",
    projectId: "cook-pocket",
    storageBucket: "cook-pocket.appspot.com",
    messagingSenderId: "699173703830",
    appId: "1:699173703830:web:5214ae44bc9444965ef2b7",
    measurementId: "G-X26VVECNQD",
  },
};
