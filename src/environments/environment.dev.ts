// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  staging: false,
  production: false,
  firebase: {
    apiKey: "AIzaSyA2sm98U9AV9MWtorC246UYJ0PQGsHe6qc",
    authDomain: "cook-pocket.firebaseapp.com",
    databaseURL: "https://cook-pocket.firebaseio.com",
    projectId: "cook-pocket",
    storageBucket: "cook-pocket.appspot.com",
    messagingSenderId: "699173703830",
    appId: "1:699173703830:web:5214ae44bc9444965ef2b7",
    measurementId: "G-X26VVECNQD",
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import "zone.js/dist/zone-error"; // Included with Angular CLI.
