import { Injectable } from "@angular/core";
import { AngularFireStorage } from "@angular/fire/storage";
import { Crop } from "@ionic-native/crop/ngx";
import { ImagePicker } from "@ionic-native/image-picker/ngx";
import { WebView } from "@ionic-native/ionic-webview/ngx";
import { ToastController } from "@ionic/angular";
import * as firebase from "firebase";

@Injectable({
  providedIn: "root",
})
export class ImageUploaderService {
  constructor(
    private imagePicker: ImagePicker,
    private toastController: ToastController,
    // private webView: WebView,
    private angularFireStorage: AngularFireStorage,
    private crop: Crop,
  ) {}

  uploadImage(imageURI: string) {
    return new Promise<any>((resolve, reject) => {
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child("image").child("imageName");
      this.encodeImageUri(imageURI, image64 => {
        imageRef.putString(image64, "data_url").then(
          snapshot => {
            resolve(snapshot.ref.getDownloadURL());
          },
          err => {
            reject(err);
          },
        );
      });
    });
  }

  private encodeImageUri(imageUri, callback) {
    const c = document.createElement("canvas");
    const ctx = c.getContext("2d");
    const img = new Image();
    img.onload = () => {
      const aux: any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      const dataURL = c.toDataURL("image/jpeg");
      callback(dataURL);
    };
    img.src = imageUri;
  }

  openImagePicker() {
    this.imagePicker.hasReadPermission().then(
      result => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        } else if (result == true) {
          this.imagePicker
            .getPictures({
              maximumImagesCount: 1,
            })
            .then(
              results => {
                for (let i = 0; i < results.length; i++) {
                  this.uploadImageToFirebase(results[i]);
                }
              },
              err => console.log(err),
            );
        }
      },
      err => {
        console.log(err);
      },
    );
  }

  private async uploadImageToFirebase(image) {
    // image = this.webView.convertFileSrc(image);
    try {
      await this.angularFireStorage.upload("x.jpg", image);

      const toast = await this.toastController.create({
        message: "Image was updated successfully",
        duration: 3000,
      });
      await toast.present();
    } catch (err) {
      console.error(err);
    }
  }

  async openImagePickerCrop() {
    this.imagePicker.hasReadPermission().then(
      result => {
        if (result == false) {
          // no callbacks required as this opens a popup which returns async
          this.imagePicker.requestReadPermission();
        } else if (result == true) {
          this.imagePicker
            .getPictures({
              maximumImagesCount: 1,
            })
            .then(
              results => {
                for (let i = 0; i < results.length; i++) {
                  this.crop.crop(results[i], { quality: 75 }).then(
                    newImage => {
                      this.uploadImageToFirebase(newImage);
                    },
                    error => console.error("Error cropping image", error),
                  );
                }
              },
              err => console.log(err),
            );
        }
      },
      err => {
        console.log(err);
      },
    );
  }
}
