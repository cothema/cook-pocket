import { ListItem } from "../model/list-item";

export function listItemFactory<Item>(item: Item): ListItem<Item> {
  return {
    item,
    shouldShow: true,
  };
}
