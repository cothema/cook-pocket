export interface ListItem<Item> {
  item: Item;
  shouldShow: boolean;
}
