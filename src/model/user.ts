import { UserModel } from "ionic-universal-firebase-login";

export class User extends UserModel {
  lang: string;
  featureLevel: number;
  hideIntroOnPocketPage: boolean = false;
  hideIntroOnMenuPage: boolean = false;
  hideIntroOnShoppingPage: boolean = false;

  public constructor(init?: Partial<User>) {
    super();
    Object.assign(this, init);
  }
}
