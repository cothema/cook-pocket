export class BasicCookItem {
  title?: string;

  constructor(partial?: Partial<BasicCookItem>) {
    Object.assign(this, partial);
  }
}
