# Cook Pocket App

[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

## Contribution

Feel free to open new issues and contribute!

## Development

### Before you start
- Copy `src/environments/environment.dev.ts` to `src/environments/environment.ts` and customize the file if needed.
- Run `npm install` to install dependencies (run it also after push from repo)

### Development server

Run `npm run dev`

### Build

Run `npm run build:browser:prod` to build the production project for browser
(see package json for other platforms).

## Deployment

### Requirements

Firebase tools:
`npm i -g firebase-tools`

Login to Firebase (and access privileges)
`firebase login`

### Deploy to Firebase

`npm run deploy:browser`

# Secret files
If you have permission to publish new mobile app releases,
you should have these secret files in `.secret/` dir:
- build.json
- cothema.p12

## WSL2 configuration

https://gist.github.com/miloshavlicek/739aadf967819bf00fcf919b843412b3
- Ionic works only with Java 8 (JDK/OpenJDK 1.8)
